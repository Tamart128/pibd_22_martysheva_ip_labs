package ru.ulstu.is.labIP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class LabIpApplication {

	public static void main(String[] args) {
		SpringApplication.run(LabIpApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s!!", name);
	}

	@GetMapping("/mult")
	public int mult(@RequestParam(value = "num1") int num1,
					@RequestParam(value = "num2") int num2) {
		return num1*num2;
	}
}
